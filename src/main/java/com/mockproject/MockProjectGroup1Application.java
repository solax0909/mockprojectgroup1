package com.mockproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockProjectGroup1Application {

    public static void main(String[] args) {
        SpringApplication.run(MockProjectGroup1Application.class, args);
    }

}
