package com.mockproject.repository.promotion;

import com.mockproject.model.promotion.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PromotionRepo extends JpaRepository<Promotion,Integer> {

}
