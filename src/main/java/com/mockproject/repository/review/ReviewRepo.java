package com.mockproject.repository.review;

import com.mockproject.model.review.Review;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepo extends JpaRepository<Review, Integer> {

}
