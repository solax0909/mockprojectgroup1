package com.mockproject.repository.role;

import com.mockproject.model.role.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role, Integer> {

}
