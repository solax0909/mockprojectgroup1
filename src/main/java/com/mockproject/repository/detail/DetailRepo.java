package com.mockproject.repository.detail;

import com.mockproject.model.detail.Order_Detail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DetailRepo extends JpaRepository<Order_Detail,Integer> {

}
