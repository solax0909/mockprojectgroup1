package com.mockproject.repository.payment;

import com.mockproject.model.payment.Payment_method;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepo extends JpaRepository<Payment_method, Integer> {
}
