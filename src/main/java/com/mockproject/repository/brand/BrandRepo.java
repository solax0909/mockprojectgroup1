package com.mockproject.repository.brand;

import com.mockproject.model.brand.Brand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandRepo extends JpaRepository<Brand,Integer> {

}
