package com.mockproject.model.promotion;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "promotions")
public class Promotion implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;
    private Integer pid;
    private String code;
    private Date start_date;
    private Date end_date;
    private Integer sales;
    private String description;

    public Promotion() {
    }

    public Promotion(Integer id, Integer pid, String code, Date start_date, Date end_date, Integer sales, String description) {
        this.id = id;
        this.pid = pid;
        this.code = code;
        this.start_date = start_date;
        this.end_date = end_date;
        this.sales = sales;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public Integer getSales() {
        return sales;
    }

    public void setSales(Integer sales) {
        this.sales = sales;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
