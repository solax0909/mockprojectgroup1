package com.mockproject.model.payment;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "payment_method")
public class Payment_method implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    public Payment_method() {
    }

    public Payment_method(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
